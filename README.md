# SmoothKeys

SmoothKeys lets you *smoothly* scroll through a Web page, in any
direction, using your arrow keys.



## Installation

You'll need to install a script manager like
[GreaseMonkey](https://www.greasespot.net/) first.

Installing SmoothKeys itself may depend on which script manager you
have, but probably you can just open the web URL of the .user.js file,
and you'll be prompted to add the script. You can get it from my
[personal website](https://andrew.tosk.in/smoothkeys.user.js).



## Usage

Scroll with the arrow keys. Hold Shift to scroll faster. The Escape key
will deselect everything.

To minimize conflict with normal web browser shortcuts, SmoothKeys only
works when no particular element in the page has focus, and nothing in
the page is highlighted. This way you don't accidentally scroll the page
while writing in a text box, or highlighting text to copy.

If you want to resume keyboard scrolling and it's not working, try
clicking on a blank part of the page to make sure the web page itself
has focus (as opposed to, say, your bookmarks toolbar or any input forms
*in* the page), and/or hit the Escape key to make sure nothing is
highlighted. You should then be able to smoothly scroll again as usual.
